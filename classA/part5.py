# deklarasi class
class KelasA:
    x = 5
    y = 'kambing'
    z = 'via'
# buat objek
ob = KelasA()
# pake si objek dan propertinya
print(ob.x)
print(ob.y)
print(ob.z)

class KelasVia:
    def __init__(self, name):
        self.name = name

oV = KelasVia('Via Valen')
print(oV.name)

ov = KelasVia('Via beneran')
print(ov.name)

# GIT